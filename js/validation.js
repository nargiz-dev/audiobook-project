function validation() {
  let form = document.getElementById("form");
  let email = document.getElementById("email");
  let button = document.querySelector(".subscribe-btn");
  let pattern = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/;
  if (email.value.match(pattern)) {
    email.classList.add("valid");
    email.classList.remove("invalid");
    button.classList.remove("disabled");
  } else {
    email.classList.remove("valid");
    email.classList.add("invalid");
    button.classList.add("disabled");
  }
  if(email.value === ""){
      email.classList.remove("valid");
      email.classList.remove("invalid");
      button.classList.remove("disabled");
  }
}
