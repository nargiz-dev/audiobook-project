const hamburger = document.querySelector(".hamburger-menu");

function openNav() {
    document.getElementById("sidebar").style.width = "100%";
    window.scrollTo(0, 0);
}

function closeNav() {
    document.getElementById("sidebar").style.width = "0";
}
hamburger.addEventListener("click", () => {
    openNav();
})