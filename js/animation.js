//bookmarks
let anchorlinks = document.querySelectorAll('a[href^="#"]');

for (let item of anchorlinks) {
  item.addEventListener("click", (e) => {
    let hashval = item.getAttribute("href");
    let target = document.querySelector(hashval);
    target.scrollIntoView({
      behavior: "smooth",
      block: "start",
    });
    history.pushState(null, null, hashval);
    e.preventDefault();
  });
}

//audiobook section animation

const elements = document.querySelectorAll(".animate3");

observer = new IntersectionObserver((entries) => {
  entries.forEach((entry) => {
    if (entry.intersectionRatio > 0) {
      entry.target.classList.add("animation");
    } else {
      entry.target.classList.remove("animation");
    }
  });
});

elements.forEach((element) => {
  observer.observe(element);
});

//features section animation

const objects = document.querySelectorAll(".animate2");

observer = new IntersectionObserver((entries) => {
  entries.forEach((entry) => {
    if (entry.intersectionRatio > 0) {
      entry.target.classList.add("features-animation");
    } else {
      entry.target.classList.remove("features-animation");
    }
  });
});

objects.forEach((object) => {
  observer.observe(object);
});

//download section animation

const downloads = document.querySelectorAll(".animate4");
observer = new IntersectionObserver((entries) => {
  entries.forEach((entry) => {
    if (entry.intersectionRatio > 0) {
      entry.target.classList.add("download-animation");
    } else {
      entry.target.classList.remove("download-animation");
    }
  });
});

downloads.forEach((download) => {
  observer.observe(download);
});

//subscribe section animation

const discounts = document.querySelectorAll(".animate1");
observer = new IntersectionObserver((entries) => {
  entries.forEach((entry) => {
    if (entry.intersectionRatio > 0) {
      entry.target.classList.add("subscribe-animation");
    } else {
      entry.target.classList.remove("subscribe-animation");
    }
  });
});
discounts.forEach((discount) => {
  observer.observe(discount);
});

const features = document.querySelectorAll(".feature");
observer = new IntersectionObserver((entries) => {
  entries.forEach((entry) => {
    if (entry.intersectionRatio > 0) {
      entry.target.classList.add("feature-animation");
    } else {
      entry.target.classList.remove("feature-animation");
    }
  });
});
features.forEach((feature) => {
  observer.observe(feature);
});



const image = document.querySelector(".image");
observer = new IntersectionObserver((entries) => {
  entries.forEach((entry) => {
    if (entry.intersectionRatio > 0) {
      entry.target.classList.add("image-animation");
    } else {
      entry.target.classList.remove("image-animation");
    }
  });
});

  observer.observe(image);



const text = document.querySelector(".header");
  observer = new IntersectionObserver((entries) => {
    entries.forEach((entry) => {
      if (entry.intersectionRatio > 0) {
        entry.target.classList.add("text-animation");
      } else {
        entry.target.classList.remove("text-animation");
      }
    });
  });
    observer.observe(text);

const footer = document.querySelector(".footer-wrapper");
observer = new IntersectionObserver((entries) => {
  entries.forEach((entry) => {
    if (entry.intersectionRatio > 0) {
      entry.target.classList.add("footer-animation");
    }
    else {
      entry.target.classList.remove("footer-animation");
    }
  });
});
observer.observe(footer);

